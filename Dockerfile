FROM ubuntu:latest
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
RUN export FLASK_APP=flaskr
RUN export FLASK_ENV=development
ENTRYPOINT ["python"]