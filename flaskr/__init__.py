import os
from flask import Flask

def create_app(test_config=None):
    # configuracion
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),
    )

    if test_config is None:
        # Cargar la instancia de aplicacion cuando no es una prueba
        app.config.from_pyfile('config.py', silent=True)
    else:
        # Cargar la configuracion de pruebas
        app.config.from_mapping(test_config)
    
    # asegurarse que la carpeta existe
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass
    
    # Correr una pagina simple de hola mundo
    @app.route('/hello')
    def hello():
        return 'Hola mundo'

    from . import db
    db.init_app(app)

    from . import auth
    app.register_blueprint(auth.bp)

    from . import blog 
    app.register_blueprint(blog.bp)
    app.add_url_rule('/', endpoint='index')
    
    return app